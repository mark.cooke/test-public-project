# Software install Script
#
# Applications to install:
#
# Foxit Reader Enterprise Packaging (requires registration)
# https://kb.foxitsoftware.com/hc/en-us/articles/360040658811-Where-to-download-Foxit-Reader-with-Enterprise-Packaging-MSI-
# 
# Notepad++
# https://notepad-plus-plus.org/downloads/v7.8.8/
# See comments on creating a custom setting to disable auto update message
# https://community.notepad-plus-plus.org/post/38160



#region Set logging 
$logFile = "c:\temp\" + (get-date -format 'yyyyMMdd') + '_softwareinstall.log'
function Write-Log {
    Param($message)
    Write-Output "$(get-date -format 'yyyyMMdd HH:mm:ss') $message" | Out-File -Encoding utf8 $logFile -Append
}
#endregion

# Install AZ CLI
try {
    Invoke-WebRequest -Uri https://aka.ms/installazurecliwindows -OutFile .\AzureCLI.msi; Start-Process msiexec.exe -Wait -ArgumentList '/I AzureCLI.msi /quiet'; rm .\AzureCLI.msi
    if (Test-Path "C:\Program Files (x86)\Microsoft SDKs\Azure\CLI2") {
        Write-Log "Azure CLI has been installed"
    }
    else {
        write-log "Error locating the Azure CLI executable"
    }
}
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error installing Azure CLI: $ErrorMessage"
}

# Download and install Datadog Agent

try {
    Invoke-WebRequest -Uri https://s3.amazonaws.com/ddagent-windows-stable/datadog-agent-7-latest.amd64.msi -OutFile .\datadog-agent-7-latest.msi; Start-Process -Wait msiexec -ArgumentList '/qn /i datadog-agent-7-latest.amd64.msi APIKEY="00771bdcc712a36c48ef4d937cab6061" SITE="datadoghq.eu"'
    if (Test-Path "C:\ProgramFiles\Datadog\Datadog Agent\bin\agent.exe") {
        Write-Log "Datadog Agent has been installed"
    }
    else {
        write-log "Error locating the Datadog Agent executable"
    }
}
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error installing Datadog Agent: $ErrorMessage"
}

# Download and install Citrix VDA


try {
    Start-Process -FilePath 'C:\temp\VDAServerSetup_2106.exe' -wait -ArgumentList '/noreboot /quiet /enable_remote_assistance /components vda,plugins /mastermcsimage /includeadditional "Citrix Supportability Tools","Machine Identity Service","Citrix MCS IODriver"'
    if (Test-Path "C:\Program Files\Citrix\Virtual Desktop Agent") {
        Write-Log "Citrix VDA has been installed"
    }
    else {
        write-log "Error locating the Citrix VDA executable"
    }
}
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error installing Citrix VDA: $ErrorMessage"
}

#region Sysprep Fix
# Fix for first login delays due to Windows Module Installer
try {
    ((Get-Content -path C:\DeprovisioningScript.ps1 -Raw) -replace 'Sysprep.exe /oobe /generalize /quiet /quit', 'Sysprep.exe /oobe /generalize /quit /mode:vm' ) | Set-Content -Path C:\DeprovisioningScript.ps1
    write-log "Sysprep Mode:VM fix applied"
}
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error updating script: $ErrorMessage"
}
#endregion

#region Time Zone Redirection
$Name = "fEnableTimeZoneRedirection"
$value = "1"
# Add Registry value
try {
    New-ItemProperty -ErrorAction Stop -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" -Name $name -Value $value -PropertyType DWORD -Force
    if ((Get-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services").PSObject.Properties.Name -contains $name) {
        Write-log "Added time zone redirection registry key"
    }
    else {
        write-log "Error locating the Teams registry key"
    }
}
catch {
    $ErrorMessage = $_.Exception.message
    write-log "Error adding teams registry KEY: $ErrorMessage"
}
#endregion